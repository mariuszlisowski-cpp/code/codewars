// simple string reversal
// ~ keeps spaces untouched

#include <algorithm>
#include <iostream>

std::string reverseOneLoop(std::string s){
    std::string rev;

    for (int i = s.size() - 1; i >= 0; --i){
        // ignore spaces in input string 
        if (s[i] != ' ') {
            rev += s[i];
        }
        // copy spaces from input
        if (s[rev.length()] == ' ') {
            rev += s[rev.size()];
        }
    }

    return rev;
}

std::string reverseTwoLoops(std::string s) {
    size_t size{ s.size() };
    std::string rev(size, '\0');
    
    for (int i = 0; i < size; ++i) {
        if (s[i] == ' ') {
            rev[i] = ' ';
        }
    }

    size_t j = size - 1;

    for (size_t i = 0; i < size; ++i) {
        // ignore spaces in input string 
        if (s[i] != ' ') {
            // ignore spaces in result
            if (rev[j] == ' ') {
                --j;
            }
            rev[j] = s[i];
            --j;
        }
    }

    return rev;
}
