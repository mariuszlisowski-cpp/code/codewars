#include "are_they_the_same.cpp"

void doTest(std::vector<int> a, std::vector<int> b, bool sol) {
    bool ans = Same::comp(a, b);
    igloo::Assert::That(ans, igloo::Equals(sol));
}

Describe(ComparisionTests) {
    std::vector<int> a;
    std::vector<int> b;

    // edge cases
    It(compareEmptyBothVectors) {
        a = {};
        b = {};
        doTest(a, b, true);
    }
    It(compareEmptyFirstVector) {
        a = { 121 };
        b = {};
        doTest(a, b, false);
    }
    It(compareEmptySecondVector) {
        a = {};
        b = { 132 };
        doTest(a, b, false);
    }
    It(compareVectorsOfDifferentSizes) {
        a = { 121, 144, 19, 161, 19, 144, 19, 11 };
        b = { 361, 20736, 362, 121 };
        doTest(a, b, false);
    }

    // true cases
    It(compareVectorsTrue_1) {
        a = { 121, 144, 19, 161, 19, 144, 19, 11 };
        b = { 14641, 20736, 361, 25921, 361, 20736, 361, 121 };
        doTest(a, b, true);
    }
    It(compareVectorsTrue_2) {
        a = { -121, 144, 19, 161, 19, 144, 19, -11 };
        b = { 14641, 20736, 361, 25921, 361, 20736, 361, 121 };
        doTest(a, b, true);
    }
    It(compareVectorsTrue_3) {
        a = { -21, -42, 0 };
        b = { 0, 441, 1764 };
        doTest(a, b, true);
    }
    It(compareVectorsTrue_4) {
        a = { 64, 38, 44, 63, 56, 70, 98, 19, 93, 53, 21, 67, 45, 21, 99, 8, 42 };
        b = { 4096, 1444, 1936, 3969, 3136, 4900, 9604, 361, 8649, 2809, 441, 4489, 2025, 441, 9801, 64, 1764 };
        doTest(a, b, true);
    }
    It(compareVectorsTrue_5) {
        a = { -121, 1440, 191, 161, 19, 144, 195, 11 };
        b = { 121, 14641, 2073600, 36481, 25921, 361, 20736, 38025 };
        doTest(a, b, true);
    }
    It(compareVectorsTrue_6) {
        a = { -2, 2, 3 };
        b = { 4, 4, 9 };
        doTest(a, b, true);
    }
    
    // false cases
    It(compareVectorsFalse_1) {
        a = { 121, 144, 19, 161, 19, 144, 19, 11 };
        b = { 14641, 20736, 361, 25921, 361, 20736, 362, 121 };
        doTest(a, b, false);
    }
    It(compareVectorsFalse_2) {
        a = { 121, 144, 19, 161, 19, 144, 19, 11 };
        b = { 132, 14641, 20736, 361, 25921, 361, 20736, 361 };
        doTest(a, b, false);
    }
    It(compareVectorsFalse_3) {
        a = { -2, 2, 3 };
        b = { 4, 9, 9 };
        doTest(a, b, false);
    }
    It(compareVectorsFalse_4) {
        a = { 21, 42, 0 };
        b = { 0, 44, 1764 };
        doTest(a, b, false);
    }
};
